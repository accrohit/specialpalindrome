package com.Microservices.TDD.Services;

import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class PalindromeService {

    public List<String> getPalindromeDetails(String inputString) {

        ArrayList<String> palindromeList = new ArrayList<String>();
        char[] charsArray = inputString.toCharArray();

        // add the individual character to the list
        for (int i = 0; i < charsArray.length; i++) {
            palindromeList.add(String.valueOf(charsArray[i]));
        }
        for (int j = 0; j < charsArray.length; j++) {
            for (int k = j+1; k < charsArray.length; k++) {
                if (charsArray[j] - charsArray[k] == 0) {
                    //add the matched value
                    palindromeList.add(inputString.substring(j, k+1));
                }
            }
        }

        palindromeList.forEach(t ->  System.out.println(t));

        return palindromeList;
    }

}