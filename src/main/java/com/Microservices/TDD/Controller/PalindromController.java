package com.Microservices.TDD.Controller;

import com.Microservices.TDD.Services.PalindromeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import java.util.List;


@RestController
@RequestMapping("/test")
public class PalindromController {

    @Autowired
    PalindromeService palindromeSvc;
    @RequestMapping("/checkPalindrome")
    public List<String> getPalindromeDetails(@RequestParam("name") String name){

       return palindromeSvc.getPalindromeDetails(name);
    }


}
