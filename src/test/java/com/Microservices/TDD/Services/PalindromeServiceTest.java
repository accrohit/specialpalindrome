package com.Microservices.TDD.Services;

import org.junit.Assert;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

//@RunWith(JunitP)
@SpringBootTest
public class PalindromeServiceTest {

    /*
        Test for special palindrome String case 1
   */
    @Autowired
    private PalindromeService palindromeService;

    @Test
        public void alternativeMatchCharacterPalindromeTest(){

        List<String> actual= palindromeService.getPalindromeDetails("abab");
        String [] expected = {"a","b","a","b","aba","bab"};
        Assert.assertArrayEquals(expected,actual.toArray());
        Assert.assertEquals(expected.length,actual.toArray().length);

    }

    /*
        Test for special palindrome String case 2
   */
    @Test
    public void repetitiveCharacterPalindromeTest(){

        List<String> actual= palindromeService.getPalindromeDetails("aaaa");
        String [] expected = {"a","a","a","a","aa","aaa","aaaa","aa","aaa","aa"};
        Assert.assertArrayEquals(expected,actual.toArray());
        Assert.assertEquals(expected.length,actual.toArray().length);

    }

    /*
     Test for non palindrome String
     */

    @Test
    public void notPalindromeFoundTest(){

        List<String> actual= palindromeService.getPalindromeDetails("abcdef");
        String [] expected = {"a","b","c","d","e","f"};
        Assert.assertArrayEquals(expected,actual.toArray());
        Assert.assertEquals(expected.length,actual.toArray().length);

    }

}
