package com.Microservices.TDD.Controller;

import com.Microservices.TDD.Services.PalindromeService;
import org.hamcrest.Matchers;
import org.junit.experimental.results.ResultMatchers;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.mockito.internal.matchers.Any;
import org.mockito.stubbing.Answer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultMatcher;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.io.PrintStream;
import java.util.ArrayList;
import java.util.List;


@ExtendWith(SpringExtension.class)
@WebMvcTest
class PalindromeControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
   private PalindromeService palindromeService;

    /*
    test execute to execute the rest service & returning the mock result
     */
    @Test
    public void palindromeControllerTest() throws Exception {
        List<String> palindromeList = new ArrayList();
        palindromeList.add("a");
        palindromeList.add("b");
        palindromeList.add("a");
        palindromeList.add("b");
        palindromeList.add("aba");
        palindromeList.add("bab");


       Mockito.when(palindromeService.getPalindromeDetails(Mockito.any())).thenReturn(palindromeList);

        mockMvc.perform(MockMvcRequestBuilders.get("/test/checkPalindrome/")
        .param("name","abab"))
        .andExpect(MockMvcResultMatchers.jsonPath("$", Matchers.hasSize(6)))
        .andExpect(MockMvcResultMatchers.status().isOk());

            }
}
